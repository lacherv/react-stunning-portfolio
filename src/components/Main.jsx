import React from "react";
import PowerButton from "./shared/PowerButton";
import { MainContainer, Container } from "./styled/Main.styled";

const Main = () => {
  return (
    <MainContainer>
      <Container>
        <PowerButton />
      </Container>
    </MainContainer>
  );
};

export default Main;
